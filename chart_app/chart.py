from django.views.generic import View
from django.core.exceptions import ImproperlyConfigured
from django.http import JsonResponse
from django.shortcuts import render
from django.core.serializers.json import DjangoJSONEncoder


class ChartBase(View):
    """
    A Base class to render Chartjs
    """

    chart_type: str = None
    options: str = ''

    def get_chart_type(self):
        if self.chart_type is None:
            raise ImproperlyConfigured(
                "'chart_type' is missing")
        else:
            return self.chart_type

    def get_queryset(self):
        raise NotImplementedError()

    def get_labels(self):
        raise NotImplementedError()

    def get_datasets(self):
        raise NotImplementedError()

    def get_options(self):
        return self.options

    def get_chart_config(self):
        """
        Build a config dictionary to match required configs in chartjs
        """

        chart_config = {
            'type': self.get_chart_type(),
            'options': self.get_options(),
            'data': {
                'labels': self.get_labels(),
                'datasets': self.get_datasets()
            }

        }
        return chart_config

    def get(self, request):
        return JsonResponse(self.get_chart_config(), encoder=DjangoJSONEncoder, safe=False)


class DashboardBase(View):
    """
        A Base class to render Dashboard
    """

    template_name: str = None
    options: str = ''
    context: dict = {}

    def get_template_name(self):

        if self.template_name is None:
            raise ImproperlyConfigured(
                "'template_name' is missing")
        else:
            return self.template_name

    def get_options(self):
        return self.options

    def get_dashboard_config(self):
        dash_config = {
            'options': self.get_options(),
        }

        return dash_config

    def get_context(self):
        return self.context

    def get(self, request):
        if request.GET.get('action') == 'get_json_data':
            return JsonResponse(self.get_dashboard_config(), encoder=DjangoJSONEncoder, safe=False)

        return render(request, self.get_template_name(), self.get_context())
