from django.db.models import Sum
from django.db.models.functions import TruncMonth

from .models import Sale
from .chart import ChartBase, DashboardBase


class SalesChart(ChartBase):
    template_name = 'line_chart.html'
    chart_type = 'line'
    url = 'sales_chart'

    options = {
        'responsive': True,
        'maintainAspectRatio': False,
        'layout': {
            'padding': {
                'left': 10,
                'right': 0,
                'top': 0,
                'bottom': 0
            }
        },
        'title': {
            'display': True,
            'text': "Mon premier graphe",
            'position': "top",
            'fontSize': 20,
            'padding': 10,
        },
        'scales': {
            'xAxes': [{
                'scaleLabel': {
                    'display': True,
                    'labelString': "Titre pour les xAxes",
                },

            }],
            'yAxes': [{
                'scaleLabel': {
                    'display': True,
                    'labelString': "Titre pour les yAxes",

                },
                'ticks': {
                    'padding': 10,
                    'beginAtZero': False,
                },
            }]
        },
    }

    def get_queryset(self):
        return Sale.objects.annotate(
            month=TruncMonth('transaction_date')
        ).values('month').annotate(quantity__sum=Sum('quantity'))

    def get_labels(self):
        return [month.strftime("%B") for month in self.get_queryset().values_list('month', flat=True)]
    
    def get_datasets(self):
        datasets = [
            {
                'label': 'Produits ' + self.request.GET.get('product'),
                'backgroundColor': 'rgba(54, 162, 235, 0.2)',
                'borderColor': 'red',
                'data': [qtt for qtt in
                         self.get_queryset().filter(
                             product__designation=self.request.GET.get('product')
                         ).values_list('quantity__sum', flat=True)],
                'fill' : True,
            }
        ]

        return datasets


class SalesChartBars(ChartBase):
    template_name = 'line_chart.html'
    chart_type = 'bar'
    url = 'sales_chart_bar'

    options = {
        'responsive': True,
        'maintainAspectRatio': False,
        'layout': {
            'padding': {
                'left': 10,
                'right': 0,
                'top': 0,
                'bottom': 0
            }
        },
        'title': {
            'display': True,
            'text': "Mon premier graphe",
            'position': "top",
            'fontSize': 20,
            'padding': 10,
        },
        'scales': {
            'xAxes': [{
                'scaleLabel': {
                    'display': True,
                    'labelString': "Titre pour les xAxes",
                },

            }],
            'yAxes': [{
                'scaleLabel': {
                    'display': True,
                    'labelString': "Titre pour les yAxes",

                },
                'ticks': {
                    'padding': 10,
                    'beginAtZero': False,
                },
            }]
        },
    }

    def get_queryset(self):
        return Sale.objects.annotate(
            month=TruncMonth('transaction_date')
        ).values('month').annotate(quantity__sum=Sum('quantity'))

    def get_labels(self):
        return [month.strftime("%B") for month in self.get_queryset().values_list('month', flat=True)]

    def get_datasets(self):
        datasets = [
            {
                'label': 'Produits ' + self.request.GET.get('product'),
                'backgroundColor': 'rgba(54, 162, 235, 0.2)',
                'borderColor': 'red',
                'data': [qtt for qtt in
                         self.get_queryset().filter(
                             product__designation=self.request.GET.get('product')
                         ).values_list('quantity__sum', flat=True)],
                
            }
        ]
        
        return datasets


class DashExample(DashboardBase):
    template_name = 'dashboard.html'

    options = {
        'charts': [
            {
                'id': 2,
                'url': SalesChartBars.url,
                'url_parameters': 'product=AA',
                'order': 1,
                'width': {'l': 4, 'm': 9, 's': 12},
                'height': {'l': 2, 'm': 2, 's': 2}
            },
            
            """{
                'id': 1,
                'url': SalesChart.url,
                'url_parameters': 'product=BB',
                'order': 1,
                'width': {'l': 4, 'm': 9, 's': 12},
                'height': {'l': 4, 'm': 2, 's': 2}
            },
            
            {
                'id': 3,
                'url': SalesChart.url,
                'url_parameters': 'product=CC',
                'order': 1,
                'width': {'l': 4, 'm': 9, 's': 12},
                'height': {'l': 2, 'm': 2, 's': 2}
            },
            {
                'id': 4,
                'url': SalesChart.url,
                'url_parameters': 'product=CC',
                'order': 1,
                'width': {'l': 4, 'm': 9, 's': 12},
                'height': {'l': 2, 'm': 2, 's': 2}
            }"""

        ]

    }
