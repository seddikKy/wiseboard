/*
var dynamic_tag = function() {

    function create_DOM_tag() {
      this.fragment_tag = document.createDocumentFragment();
    }
      create_DOM_tag.prototype.parent_tag = function(parent_tag, index) {
        this.parent_tag = document.getElementsByTagName(parent_tag)[index];
        console.log(this.parent_tag)      
      }
      create_DOM_tag.prototype.type_tag = function(type, type_tag) {
        tag = document.createElement(type);
        this.type_tag = type;
        this.fragment_tag.appendChild(tag);
      }

      create_DOM_tag.prototype.content_tag = function(content_tag) {
        this.content_tag = content_tag;
        tag.textContent  = content_tag;
      }
      create_DOM_tag.prototype.id_tag = function(id_tag) {
        this.id_tag = id_tag;
        tag.id = id_tag;
      }
      create_DOM_tag.prototype.class_tag = function(class_tag) {
        this.class_tag = class_tag;
        tag.className = class_tag;
      }
      create_DOM_tag.prototype.style_tag = function(style_tag) {
        this.style_tag = style_tag;
        tag.style = style_tag;
      }
      create_DOM_tag.prototype.form_action_tag = function(form_action_tag) {
        this.form_action_tag = form_action_tag;
        tag.action = form_action_tag;
      }
      create_DOM_tag.prototype.form_target_tag = function(form_target_tag) {
        this.form_target_tag = form_target_tag;
        tag.target = form_target_tag;
      }
      create_DOM_tag.prototype.input_type_tag = function(input_type_tag) {
        this.input_type_tag = input_type_tag;
        tag.type = input_type_tag;
      }
      
      create_DOM_tag.prototype.make_tag = function() {
        this.parent_tag.appendChild(this.fragment_tag);
      }
   
    return { create: create_DOM_tag }
  }();
  var div = new dynamic_tag.create();
    div.parent_tag("body", 0);
    div.type_tag("DIV");
    div.class_tag("w3-container");
    //div.id_tag("filter_du_id");
    div.make_tag();
*/

var dashboard = function() {
        
        function create_DOM_dashboard(){
        
            this.fragment_tag = document.createDocumentFragment();
            this.parent_tag = document.getElementsByTagName("body")[0];
          
                //NAVBAR
                
                div = document.createElement('DIV');
                div.className = "w3-top w3-mobile"
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);
                  
                this.parent_tag = document.getElementsByTagName("body")[0];
                div = document.createElement('DIV');
                div.className = "w3-bar w3-black w3-mobile";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("div")[1];
                span = document.createElement('SPAN');
                span.className = "branding w3-bar-item w3-mobile";
                this.fragment_tag.appendChild(span);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("div")[1];
                span = document.createElement('SPAN');
                span.className = "w3-right w3-mobile";
                this.fragment_tag.appendChild(span);
                this.parent_tag.appendChild(this.fragment_tag);
            
                this.parent_tag = document.getElementsByTagName("span")[0];
                a = document.createElement('A');
                a.className = "w3-bar-item w3-button w3-mobile w3-hover-red";
                a.textContent = "DASHBOARD"
                this.fragment_tag.appendChild(a);
                this.parent_tag.appendChild(this.fragment_tag);
            
                //FILTRAGE

                this.parent_tag = document.getElementsByTagName("body")[0];
                div = document.createElement('div');
                div.className = "w3-row w3-mobile";
                div.id = "div-row-id"
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-row-id");
                section = document.createElement('SECTION');
                section.className = "section w3-mobile";
                this.fragment_tag.appendChild(section);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("section")[0];
                div = document.createElement('DIV');
                div.className = "w3-col s3 m12 l3 w3-container w3-mobile";
                div.id = "div-section-1";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);
        
                this.parent_tag = document.getElementById("div-section-1");
                div = document.createElement('DIV');
                div.className = "w3-card-4 w3-mobile";
                div.id = "div-section-2";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-section-2");
                div = document.createElement('DIV');
                div.className = "w3-container w3-dark-grey w3-mobile";
                div.id = "div-section-3";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-section-3");
                h = document.createElement('H');
                h.textContent = "FILTRES :"  
                this.fragment_tag.appendChild(h);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-section-2");
                form = document.createElement('FORM');
                form.className = "w3-container  w3-padding-large w3-mobile";
                this.fragment_tag.appendChild(form);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("form")[0];
                label = document.createElement('LABEL');
                label.className = "w3-label w3-mobile";
                label.textContent = "DU :"
                this.fragment_tag.appendChild(label);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("form")[0];
                input = document.createElement('INPUT');
                input.className = "w3-input w3-mobile";
                input.type = "text"
                this.fragment_tag.appendChild(input);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("form")[0];
                label = document.createElement('LABEL');
                label.className = "w3-label w3-mobile";
                label.textContent = "AU :"
                this.fragment_tag.appendChild(label);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("form")[0];
                input = document.createElement('INPUT');
                input.className = "w3-input w3-mobile";
                input.type = "text"
                this.fragment_tag.appendChild(input);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-section-2");
                div = document.createElement('DIV');
                div.className = "w3-container w3-mobile";
                div.id = "div-op-1"
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                //OPTIONS

                this.parent_tag = document.getElementById("div-section-2");
                div = document.createElement('DIV');
                div.className = "w3-container w3-dark-grey w3-mobile";
                div.id = "div-section-4";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-section-4");
                h = document.createElement('H');
                h.textContent = "OPTIONS :"  
                this.fragment_tag.appendChild(h);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-section-2");
                div = document.createElement('DIV');
                div.className = "w3-container s12 m3 l3 w3-mobile";
                div.id = "div-section-5";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-section-5");
                form = document.createElement('FORM');
                form.className = "w3-container  w3-padding-large w3-mobile";
                form.id = "form-id-1"
                this.fragment_tag.appendChild(form);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("form-id-1");
                label = document.createElement('LABEL');
                label.className = "w3-label w3-mobile";
                label.textContent = "VENTES :"
                this.fragment_tag.appendChild(label);
                this.parent_tag.appendChild(this.fragment_tag);

                //this.parent_tag = document.getElementById("form-id-1");
                input = document.createElement('INPUT');
                input.className = "w3-input w3-mobile";
                input.type = "text"
                this.fragment_tag.appendChild(input);
                this.parent_tag.appendChild(this.fragment_tag);

                //this.parent_tag = document.getElementById("form-id-1");
                label = document.createElement('LABEL');
                label.className = "w3-label w3-mobile";
                label.textContent = "PRODUITS :"
                this.fragment_tag.appendChild(label);
                this.parent_tag.appendChild(this.fragment_tag);

                //this.parent_tag = document.getElementById("form-id-1");
                input = document.createElement('INPUT');
                input.className = "w3-input w3-mobile";
                input.type = "text"
                this.fragment_tag.appendChild(input);
                this.parent_tag.appendChild(this.fragment_tag);

                //BUTTON
                button = document.createElement('button');
                button.className = "w3-button w3-blue w3-hover-dark-grey w3-mobile";
                button.textContent = "APPLIQUER"
                this.fragment_tag.appendChild(button);
                this.parent_tag.appendChild(this.fragment_tag);

                //CANVAS - CHART-1

                this.parent_tag = document.getElementsByTagName("body")[0];
                section = document.createElement('SECTION');
                section.className = "section w3-mobile";
                this.fragment_tag.appendChild(section);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("section")[0];
                div = document.createElement('DIV');
                div.className = "w3-col s3 m6 l3 w3-container w3-mobile";
                div.id = "div-canvas-id-ch1-1"
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch1-1");
                div = document.createElement('DIV');
                div.className = "w3-card-4 w3-mobile";
                div.id = "div-canvas-id-ch1-2";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch1-2");
                canvas = document.createElement('CANVAS');
                canvas.id = "myChart1";
                canvas.style.height = "250px"
                this.fragment_tag.appendChild(canvas);
                this.parent_tag.appendChild(this.fragment_tag);

                //CANVAS - CHART-2
                this.parent_tag = document.getElementsByTagName("body")[0];
                section = document.createElement('SECTION');
                section.className = "section w3-mobile";
                this.fragment_tag.appendChild(section);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("section")[0];
                div = document.createElement('DIV');
                div.className = "w3-col s3 m6 l3 w3-container w3-mobile";
                div.id = "div-canvas-id-ch2-1"
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch2-1");
                div = document.createElement('DIV');
                div.className = "w3-card-4 w3-mobile";
                div.id = "div-canvas-id-ch2-2";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch2-2");
                canvas = document.createElement('CANVAS');
                canvas.id = "myChart2";
                canvas.style.height = "250px";
                this.fragment_tag.appendChild(canvas);
                this.parent_tag.appendChild(this.fragment_tag);

                //CANVAS - CHART-3
                this.parent_tag = document.getElementsByTagName("body")[0];
                section = document.createElement('SECTION');
                section.className = "section w3-mobile";
                this.fragment_tag.appendChild(section);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("section")[0];
                div = document.createElement('DIV');
                div.className = "w3-col s3 m6 l3 w3-container w3-mobile";
                div.id = "div-canvas-id-ch3-1"
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch3-1");
                div = document.createElement('DIV');
                div.className = "w3-card-4 w3-mobile";
                div.id = "div-canvas-id-ch3-2";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch3-2");
                canvas = document.createElement('CANVAS');
                canvas.id = "myChart3";
                canvas.style.height = "500px";
                this.fragment_tag.appendChild(canvas);
                this.parent_tag.appendChild(this.fragment_tag);

                //CANVAS - CHART-4
                
                this.parent_tag = document.getElementsByTagName("body")[0];
                section = document.createElement('SECTION');
                section.className = "section w3-mobile";
                section.id = "section1"
                this.fragment_tag.appendChild(section);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("section")[0];
                div = document.createElement('DIV');
                div.className = "w3-col s3 m6 l3 w3-container w3-mobile";
                div.id = "div-canvas-id-ch4-1"
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch1-1");
                div = document.createElement('DIV');
                div.className = "w3-card-4 w3-mobile";
                div.id = "div-canvas-id-ch4-2";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch4-2");
                canvas = document.createElement('CANVAS');
                canvas.id = "myChart4";
                canvas.style.height = "250px"
                this.fragment_tag.appendChild(canvas);
                this.parent_tag.appendChild(this.fragment_tag);

                //CANVAS - CHART 5
                this.parent_tag = document.getElementsByTagName("body")[0];
                section = document.createElement('SECTION');
                section.className = "section w3-mobile";
                this.fragment_tag.appendChild(section);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementsByTagName("section")[0];
                div = document.createElement('DIV');
                div.className = "w3-col s3 m6 l3 w3-container w3-mobile";
                div.id = "div-canvas-id-ch5-1"
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch2-1");
                div = document.createElement('DIV');
                div.className = "w3-card-4 w3-mobile";
                div.id = "div-canvas-id-ch5-2";
                this.fragment_tag.appendChild(div);
                this.parent_tag.appendChild(this.fragment_tag);

                this.parent_tag = document.getElementById("div-canvas-id-ch5-2");
                canvas = document.createElement('CANVAS');
                canvas.id = "myChart5";
                canvas.style.height = "250px";
                this.fragment_tag.appendChild(canvas);
                this.parent_tag.appendChild(this.fragment_tag);
                
            }
      
          return { create: create_DOM_dashboard } 
        
        }();

